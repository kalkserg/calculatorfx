package calapp;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class InterfaceController {

    @FXML
    private TextField display;

    @FXML
    private Button seven;

    @FXML
    private Button eigth;

    @FXML
    private Button five;

    @FXML
    private Button nine;

    @FXML
    private Button add;

    @FXML
    private Button six;

    @FXML
    private Button four;

    @FXML
    private Button sub;

    @FXML
    private Button one;

    @FXML
    private Button two;

    @FXML
    private Button three;

    @FXML
    private Button mul;

    @FXML
    private Button pn;

    @FXML
    private Button zero;

    @FXML
    private Button dot;

    @FXML
    private Button div;

    @FXML
    private Button erase;

    @FXML
    private Button equal;

    double result;
    double n1;
    double n2;
    char op;
    int point;
    boolean isPressEq = false;

    @FXML
    private void calculation(ActionEvent event){
        if(isPressEq) {display.setText("");isPressEq = false;}
        if(event.getSource() == one){
            display.appendText("1");
        }else
        if(event.getSource() == two){
            display.appendText("2");
        }else
        if(event.getSource() == three){
            display.appendText("3");
        }else
        if(event.getSource() == four){
            display.appendText("4");
        }else
        if(event.getSource() == five){
            display.appendText("5");
        }else
        if(event.getSource() == six){
            display.appendText("6");
        }else
        if(event.getSource() == seven){
            display.appendText("7");
        }else
        if(event.getSource() == eigth){
            display.appendText("8");
        }else
        if(event.getSource() == nine){
            display.appendText("9");
        }else
        if(event.getSource() == zero){
            display.appendText("0");
        }else
        if(event.getSource() == dot && point == 0 ){
            display.appendText(".");
            point = 1;
        }else
        if(event.getSource() == add){
            n1 = Double.parseDouble(display.getText());
            display.setText("");
            op = '+';
            point = 0;
        }else
        if(event.getSource() == sub){
            n1 = Double.parseDouble(display.getText());
            display.setText("");
            op = '-';
            point = 0;
        }else
        if(event.getSource() == mul){
            n1 = Double.parseDouble(display.getText());
            display.setText("");
            op = '*';
            point = 0;
        }else
        if(event.getSource() == div){
            n1 = Double.parseDouble(display.getText());
            display.setText("");
            op = '/';
            point = 0;
        }else
        if(event.getSource() == pn){
            n1 = Double.parseDouble(display.getText());
            n1 = n1 * (-1);
            display.setText(String.valueOf(n1));
            point = 0;
        }else
        if(event.getSource() == equal){
            isPressEq = true;
            try {
                n2 = Double.parseDouble(display.getText());
                switch (op) {
                    case '+':
                        result = n1 + n2;
                        break;
                    case '-':
                        result = n1 - n2;
                        break;
                    case '*':
                        result = n1 * n2;
                        break;
                    case '/':
                        result = n1 / n2;
                        break;
                    default:
                        display.setText("Error");
                }
                display.setText(String.valueOf(result));
            }catch (Exception e){
                display.setText("Enter number!");
            }
        }else
        if(event.getSource() == erase){
            n1 = 0;
            n2 = 0;
            display.setText("");
            point = 0;
            op = 0;
            result = 0;
        }
    }
}